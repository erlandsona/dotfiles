# Personal tmux bindings

# Generic Commands {{{
#
# First remove *all* keybindings
unbind -a
# Now reinsert all the regular tmux keys with the -n flag for no prefix.
set  -g   prefix M-Space # setup prefix key just in case.
bind     M-Space send-prefix # C-b then C-b again to send C-b
bind         C-l send-keys 'C-l' # Use C-a C-l to clear screen if something isn't right.
bind -n      M-? list-keys # HELP! Keyboard-Shorcuts
bind -n      M-: command-prompt

# Force reload of config file
bind -n      M-r source-file ~/.tmux.conf \; display-message "Config reloaded..."
bind -n      M-I run-shell ~/.tmux/plugins/tpm/bin/install_plugins
bind -n      M-U run-shell "~/.tmux/plugins/tpm/bin/update_plugins all"
bind -n      M-C-u run-shell ~/.tmux/plugins/tpm/bin/clean_plugins
#
# }}}

# Window Commands: EG. Tmux Tabs {{{
#
bind -n      M-c command-prompt -p "Name of new window: " "new-window -c '#{pane_current_path}' -n '%%'"
bind -n      M-, command-prompt -I "#W" "rename-window '%%'"
bind -n      M-n next-window
bind -n      M-p previous-window
bind -n      M-P swap-window -t -1
bind -n      M-N swap-window -t +1
bind -n   M-Left swap-window -t -1
bind -n  M-Right swap-window -t +1
bind -n      M-W confirm-before -p "kill-window #W? (y/n)" kill-window
#
# }}}

# Pane Commands: EG. Splits {{{
#
bind -n      M-! break-pane # Moves current pane to new window.
bind -n      M-w confirm-before -p "kill-pane #P? (y/n)" kill-pane
bind -n      M-o split-window -v -c "#{pane_current_path}"
bind -n      M-v split-window -h -c "#{pane_current_path}"
bind -n      M-{ swap-pane -U
bind -n      M-} swap-pane -D
bind -n      M-z resize-pane -Z # Focus current pane.
# Vim-Tmux-Navigator for splits.
is_vim='echo "#{pane_current_command}" | grep -iqE "(^|\/)g?(view|n?vim?)(diff)?$"'
bind -n      M-h if-shell "$is_vim" "send-keys M-h" "select-pane -L"
bind -n      M-j if-shell "$is_vim" "send-keys M-j" "select-pane -D"
bind -n      M-k if-shell "$is_vim" "send-keys M-k" "select-pane -U"
bind -n      M-l if-shell "$is_vim" "send-keys M-l" "select-pane -R"
# bind -n      M-\ if-shell "$is_vim" "send-keys M-\\" "select-pane -l"
bind -n      M-H resize-pane -L 2
bind -n      M-J resize-pane -D 2
bind -n      M-K resize-pane -U 2
bind -n      M-L resize-pane -R 2
bind -n      M-1 select-layout even-horizontal
bind -n      M-2 select-layout even-vertical
bind -n      M-3 select-layout main-horizontal
bind -n      M-4 select-layout main-vertical
bind -n      M-5 select-layout tiled
bind -n      M-= select-layout tiled
bind -n  M-Space next-layout
#
# }}}

# Session Commands {{{
#
bind -n    M-'$' command-prompt -I "#S" "rename-session '%%'"
bind -n      M-( switch-client -p # Move to previous session
bind -n      M-) switch-client -n # Move to next session
bind -n      M-C run "~/.tmux/plugins/tmux-sessionist/scripts/new_session_prompt.sh"
bind -n      M-X run "~/.tmux/plugins/tmux-sessionist/scripts/kill_session_prompt.sh '#{session_name}' '#{session_id}'"
bind -n      M-g run "~/.tmux/plugins/tmux-sessionist/scripts/goto_session.sh"
bind -n      M-s run "~/.tmux/plugins/tmux-resurrect/scripts/save.sh"
# Redraw the client (if interrupted by wall, etc)
bind -n      M-R refresh-client
bind -n      M-d detach-client
#
# }}}

# Copy Mode Commands {{{
#
bind -n      M-[ copy-mode
bind -n      M-] paste-buffer

# Copy-paste integration
# set  -g           default-shell "reattach-to-user-namespace -l bash"
set  -g           default-command "bash"
# Setup 'v' to begin selection as in Vim
setw -g mode-keys vi
bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"
bind -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"

# bind -t vi-copy y copy-pipe "pbcopy"
# Update default binding of `Enter` to also use copy-pipe
# unbind -t vi-copy Enter
# bind -t   vi-copy Enter copy-pipe "pbcopy"



bind -n       M-q display-panes
bind -n       M-t clock-mode
bind -n       M-~ show-messages
# }}}

# Uncomment if default bindings are prefered.
# source-file ~/.tmux.reset.conf

# Other Tmux customizations...

set -g history-limit 10000

# Mouse Mappings {{{
setw -g xterm-keys on

# Allow better scrolling when mouse focuses on inactive window.
set -g focus-events on

# Fix Escape key glitchyness
set -gs escape-time 1
set -g mouse on

setw -g mode-keys vi
set  -g status-keys vi

bind -T copy-mode-vi WheelUpPane select-pane \; send-keys -X -N 1 scroll-up
bind -T copy-mode-vi WheelDownPane select-pane \; send-keys -X -N 1 scroll-down

# }}}


# UI {{{
#
# Window/Pane #'s start at 1
set  -g base-index 1
setw -g pane-base-index 1

# renumber windows when a window is closed
set  -g renumber-windows on

# Activity Monitor
setw -g monitor-activity on
set  -g visual-activity on
set  -g bell-action any
set  -g visual-bell off

# Automatically set window titles
set  -g set-titles on
set  -g set-titles-string '#h ❐ #S ● #I #W'

# Show performance counters in statusbar
# Requires https://github.com/thewtex/tmux-mem-cpu-load/
# Caution before using anything that will require iTerm to re-draw the screen too often.
# caused opendirectoryd to hog cpu to 100% on 15"MBP
set  -g status-interval 30
set  -g status-position top

# Rather than constraining window size to the maximum size of any client
# connected to the *session*, constrain window size to the maximum size of any
# client connected to *that window*. Much more reasonable.
setw -g aggressive-resize on

# This tmux statusbar config was created by tmuxline.vim
# on Tue, 14 Jul 2015

# Terminal Color Setting
set  -g default-terminal "screen-256color"

# Statusbar Color Palatte
set  -g status-bg "colour234"
set  -g message-command-fg "colour255"
set  -g status-justify "centre" # center not a property
set  -g status-left-length "100"
set  -g status "on"
# Pane Border Color Palette
set  -g pane-active-border-fg "colour190"
set  -g message-bg "colour238"
set  -g status-right-length "100"
set  -g status-right-attr "none"
# Message Color Palette
set  -g message-fg "colour255"
set  -g message-command-bg "colour238"
set  -g status-attr "none"
set  -g pane-border-fg "colour238"
set  -g status-left-attr "none"
# Window Status Color Palette
setw -g window-status-fg "colour85"
setw -g window-status-attr "none"
setw -g window-status-activity-bg "colour234"
setw -g window-status-activity-attr "none"
setw -g window-status-activity-fg "colour190"
setw -g window-status-separator " "
setw -g window-status-bg "colour234"
# Window Status Config
set  -g status-left "#[fg=colour17,bg=colour190] #S #[fg=colour190,bg=colour234,nobold,nounderscore,noitalics]"
set  -g status-right "#[fg=colour85,bg=colour234] %b %d %Y, %H:%M #[fg=colour190,bg=colour234,nobold,nounderscore,noitalics]#[fg=colour17,bg=colour190] Austin Erlandson "
setw -g window-status-format "#[fg=colour85,bg=colour234] #I #W #P "
setw -g window-status-current-format "#[bg=colour234,fg=colour238,nobold,nounderscore,noitalics]#[fg=colour190,bg=colour238]#I #W #P#[bg=colour234,fg=colour238,nobold,nounderscore,noitalics]"
# auto window rename
setw -g automatic-rename
#
# }}}

# Plugins {{{
#
# prefix + I to install
# prefix + U to update
# List of plugins
set  -g @plugin 'tmux-plugins/tpm'

set  -g @plugin 'tmux-plugins/tmux-sessionist'

set  -g @plugin 'tmux-plugins/tmux-resurrect'
set  -g @resurrect-strategy-vim 'session'
set  -g @resurrect-capture-pane-contents 'on'
set  -g @resurrect-save-bash-history 'on'
set  -g @resurrect-processes 'false'

# Restore TMUX on boot (Continuum)
set  -g @plugin 'tmux-plugins/tmux-continuum'
# set  -g @continuum-boot 'on'
# set  -g @continuum-boot-options 'iterm,fullscreen'
set  -g @continuum-save-interval '720'
set  -g @continuum-restore 'on'

# set  -g @plugin 'tmux-plugins/tmux-open'
set  -g @plugin 'tmux-plugins/tmux-yank' # Cross platform copy/paste?

# set  -g @plugin 'nhdaly/tmux-better-mouse-mode'
# set  -g @emulate-scroll-for-no-mouse-alternate-buffer 'on'
# set  -g @scroll-speed-num-lines-per-scroll '1'

if "test ! -d ~/.tmux/plugins/tpm" "run 'git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm'"
# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
#
# }}}

# vim:foldmethod=marker:foldlevel=0
