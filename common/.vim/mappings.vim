" Leader
let mapleader = ','

"###########################
" Remap : to ;
"###########################
nnoremap ; :


"###########################
" Clear highlighting on escape in normal mode
"###########################
nnoremap <Leader>/ :noh<return><esc>

"###########################
" format the entire file
"###########################
nnoremap <Leader>= ggVG=

"###########################
" Set control-d/u to only scroll 5 lines
"###########################
nnoremap <c-u> 5k
nnoremap <c-d> 5j
nnoremap <c-b> 20k
nnoremap <c-f> 20j
vnoremap <c-u> 5k
vnoremap <c-d> 5j
vnoremap <c-b> 20k
vnoremap <c-f> 20j

"###########################
" Set mouse scrolling more sensible.
"###########################
map <ScrollWheelUp> <c-y>
map <ScrollWheelDown> <c-e>

"###########################
" Search for selected text, forwards or backwards.
"###########################
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
