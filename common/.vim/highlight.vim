hi IndentGuidesOdd ctermbg=black
hi IndentGuidesEven ctermbg=233

"###########################
" Vim Diff colors.
"###########################
hi DiffAdd cterm=none ctermfg=10 ctermbg=none guifg=bg guibg=green gui=none
hi DiffDelete cterm=none ctermfg=9 ctermbg=none guifg=bg guibg=red gui=none
hi DiffChange cterm=none ctermfg=13 ctermbg=none guifg=bg guibg=yellow gui=none
hi DiffText cterm=none ctermfg=208 ctermbg=none guifg=bg guibg=magenta gui=none
hi VertSplit term=none cterm=none ctermbg=none guibg=none
hi StatusLine term=none cterm=none ctermbg=none guibg=none
hi StatusLineNC term=none cterm=none ctermbg=none guibg=none
hi Search ctermfg=none ctermbg=235 guibg=#808080
hi LineNr ctermfg=5
hi CursorLineNr ctermfg=11
"###########################
" Highlight Links
"###########################
hi multiple_cursors_cursor term=reverse cterm=reverse gui=reverse
hi! link Visual Search
hi! link Folded Search
hi link multiple_cursors_visual Search


" ALE Linting colors
hi SpellBad ctermbg=red ctermfg=white


" 80 Column - Not working because it conflicts with vim-better-whitespace
" hi OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
" function! OverLength()
"   3match OverLength /\%>80v.\+/
" endfunction

" call OverLength()
" augroup OverLength
"   autocmd!
"   autocmd BufEnter,BufWrite
"         \,CursorMoved,CursorMovedI
"         \,InsertEnter,InsertLeave
"         \ cs
"         \,elm,haskell
"         \,javascript,javascript.jsx,js
"         \,json
"         \,php
"         \,python
"         \,ruby
"         \,vim
"         \ call OverLength()
" augroup END
