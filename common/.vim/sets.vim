set autoindent
set autoread
set backspace=indent,eol,start
set backup writebackup
set backupcopy=yes
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set clipboard=unnamed
" Remove 'Press Enter to continue' message when type information is longer than one line.
set cmdheight=2
set complete-=i
set encoding=utf-8
set expandtab



" Default status line...
" Split window bar fillchars and highlight so the split bar looks nicer:)
set fillchars=stl:─,stlnc:─,vert:│
set laststatus=2  " 0=never,1=for 2+ windows,default is 2=always

set foldenable
set foldlevel=10
set foldlevelstart=10
set foldnestmax=10
set foldmethod=indent
if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j " Delete comment character when joining commented lines
endif
" set guicursor=  " Fixes `sudo nvim /file/path` from outputing spurios q's
set hlsearch
set incsearch
set ignorecase
set lazyredraw
set modelines=1
set mouse=a
set mousefocus
set nocompatible
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif
" Don't ask to save when changing buffers (i.e. when jumping to a type definition)
" For ctrl-space
" set hidden

" For Omnisharp stuff?
" Don't hide buffers causing them to build up in memory.
" Instead when leaving a file with changes decide whether or not to save those changes.
set nohidden

set noshowmode
set noswapfile
set nowrap
set nrformats-=octal
set number
set path+=**
" set rnu
if !&scrolloff
  set scrolloff=1
endif
set sessionoptions-=options
if !&sidescrolloff
  set sidescrolloff=5
endif

" Defaults
set shiftwidth=2
set tabstop=2
" shiftwidth&tabstop=2

autocmd BufNewFile,BufRead *.axlsx setlocal filetype=ruby
autocmd FileType html,ruby,rspec,eruby,yml,yaml,javascript,json,cson,js,coffee,cabal setlocal shiftwidth=2 tabstop=2
" shiftwidth&tabstop=4
autocmd FileType elm,haskell,javascript.jsx,cs,markdown,sass,php,blade setlocal shiftwidth=4 tabstop=4
" autocmd FileType javascript setlocal shiftwidth=4 tabstop=4

set shiftround
" set showmatch
set noshowmatch " Suggested by omnisharp-vim.
set smartcase
set smarttab
set splitbelow
set splitright
set synmaxcol=300
syntax sync minlines=300
" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^linux\|^Eterm'
  set t_Co=16
endif
set ttimeout
set ttimeoutlen=100
set ttyfast
if has("mouse_sgr")
  set ttymouse=sgr
end
if !has('nvim')
  set ttymouse=xterm2
end
set updatetime=500
if !empty(&viminfo)
  set viminfo^=!
endif
set wildmenu
