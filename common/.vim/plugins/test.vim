Plug 'janko-m/vim-test'
"###########################
" Vim-Test mappings
"###########################
" let test#strategy = "basic" " default is :!
" let g:test#runner_commands = ['Minitest']
nmap <silent> <Leader>t :TestNearest<CR>
nmap <silent> <Leader>T :TestFile<CR>
nmap <silent> <Leader>a :TestSuite<CR>
nmap <silent> <Leader>l :TestLast<CR>
nmap <silent> <Leader>g :TestVisit<CR>
