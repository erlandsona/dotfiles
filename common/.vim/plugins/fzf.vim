Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" FZF, CtrlP, Ag, Etc...
nnoremap <c-t> :Tags<return>
nnoremap <c-p> :GFiles -co --exclude-per-directory=.gitignore<return>
nnoremap <c-\> :Ag <c-r><c-w><CR>
nnoremap <Leader><bslash> :Ag<Space>
