Plug 'christoomey/vim-tmux-navigator'
"##############################################################################
" Easier split navigation - See vim-tmux-navigator
"##############################################################################

" Use option-[hjkl] to select the active split!
" For international languages use right option.
let g:tmux_navigator_no_mappings = 1
" Disable tmux navigator when zooming the Vim pane
let g:tmux_navigator_disable_when_zoomed = 1

if has('nvim')
  nnoremap <silent> <M-h> :TmuxNavigateLeft<cr>
  nnoremap <silent> <M-j> :TmuxNavigateDown<cr>
  nnoremap <silent> <M-k> :TmuxNavigateUp<cr>
  nnoremap <silent> <M-l> :TmuxNavigateRight<cr>
else
  nnoremap <silent> <Esc>h :TmuxNavigateLeft<cr>
  nnoremap <silent> <Esc>j :TmuxNavigateDown<cr>
  nnoremap <silent> <Esc>k :TmuxNavigateUp<cr>
  nnoremap <silent> <Esc>l :TmuxNavigateRight<cr>
endif
" nnoremap <silent> <BS> :TmuxNavigateLeft<cr>
" nnoremap <Esc>\ :TmuxNavigatePrevious<cr>


