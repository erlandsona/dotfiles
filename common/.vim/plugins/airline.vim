Plug 'bling/vim-airline'

"###########################
" Airline Implementation...
"###########################
let g:airline_powerline_fonts = 1
" let g:airline_inactive_alt_sep=1
" let g:airline_inactive_collapse=1
let g:airline_left_sep = ""
let g:airline_right_sep = " "
" let w:airline_disabled = 1
" let g:airline_focuslost_inactive = 1
let g:airline#extensions#tabline#left_sep = ""
let g:airline#extensions#tabline#left_alt_sep = ""
let g:airline#extensions#branch#displayed_head_limit = 15
let g:airline#extensions#default#layout = [
  \ [ 'a', 'b', 'c' ],
  \ [ 'x', 'z', 'warning' ]
  \ ]
let g:airline#extensions#default#section_truncate_width = {
  \ 'b': 80,
  \ 'x': 100,
  \ 'z': 60,
  \ }

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#tabline#show_splits = 0
" let g:airline#extensions#tabline#buffer_min_count = 0
" Note: this setting only applies to a single tab and when `show_buffers` is true.
let g:airline#extensions#tabline#tab_min_count = 2
" Note: this setting only applies when `show_buffers` is false.
" let g:airline#extensions#tabline#tab_nr_type = 1 " splits and tab number
let g:airline#extensions#tabline#show_tab_nr = 0
" let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#formatter = 'unique_tail_improved' " options are 'default', 'unique_tail', 'uniqure_tail_improved'
" let g:airline#extensions#tabline#buffer_nr_show = 0
" let g:airline#extensions#tabline#buffer_nr_format = '%s: '
" let g:airline#extensions#tabline#fnamemod = ':r:r:r'
let g:airline#extensions#tabline#fnamecollapse = 0
" let g:airline#extensions#tabline#fnametruncate = 0
let g:airline#extensions#tabline#show_close_button = 1
let g:airline#extensions#tabline#close_symbol = 'ⓧ'
let airline#extensions#tabline#ignore_bufadd_pat = '\c\vgundo|undotree|vimfiler|tagbar|nerd_tree'

" let g:airline#extensions#ctrlspace#enabled = 1
" let g:airline#extensions#tmuxline#enabled = 0
