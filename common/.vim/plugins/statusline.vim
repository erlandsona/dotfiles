set encoding=utf-8

hi Modified guifg=red ctermfg=red

function! RedrawStatuslineColors(mode)
  if a:mode == 'n'
    call NormalHighlight()
  elseif a:mode == 'i'
    call InsertHighlight()
  elseif a:mode == 'R'
    call ReplaceHighlight()
  elseif a:mode == 'v' || a:mode == 'V' || a:mode == ''
    call VisualHighlight()
  endif
  return ""
endfunction

function! NormalHighlight()
  hi PathColor guibg=darkblue ctermbg=darkblue
  hi IconColor guibg=none guifg=darkblue ctermbg=none ctermfg=darkblue
endfunction

function! InsertHighlight()
  hi PathColor guibg=darkmagenta ctermbg=darkmagenta
  hi IconColor guibg=none guifg=darkmagenta ctermbg=none ctermfg=darkmagenta
endfunction

function! ReplaceHighlight()
  hi PathColor guibg=darkred ctermbg=darkred
  hi IconColor guibg=none guifg=darkred ctermbg=none ctermfg=darkred
endfunction

function! VisualHighlight()
  hi PathColor guibg=darkgreen ctermbg=darkgreen
  hi IconColor guibg=none guifg=darkgreen ctermbg=none ctermfg=darkgreen
endfunction

function! StatusLineEnter()
  let statusline="%{RedrawStatuslineColors(mode())}"
  let statusline.="%#PathColor#%< %f %#IconColor# "

  let statusline.=&mod==1 ? "%#Modified#%m" : ""


  let statusline.="%R%*"

  if exists('b:term_title')
    return ""
  else
    return statusline
  endif
endfunction

function! StatusLineLeave()
  return "%#Modified#%m%#StatusLine#"
endfunction

call NormalHighlight()

set statusline=%!StatusLineEnter()
augroup statusline
  autocmd!
  au WinEnter * setlocal statusline=%!StatusLineEnter()
  au WinLeave * setlocal statusline=%!StatusLineLeave()
augroup END
