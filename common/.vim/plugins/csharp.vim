Plug 'OrangeT/vim-csharp', { 'for': 'cs' }
Plug 'OmniSharp/omnisharp-vim', { 'for': 'cs', 'do': 'git submodule update --init --recursive && cd server && xbuild' }

"###########################
" OmniSharp
"###########################
let g:OmniSharp_timeout = 1
let g:OmniSharp_server_type = 'roslyn'

" Enable snippet completion, requires completeopt-=preview
let g:OmniSharp_want_snippet=1

" Contextual code actions (requires CtrlP or unite.vim)
nnoremap <Leader><space> :OmniSharpGetCodeActions<cr>
" Run code actions with text selected in visual mode to extract method
vnoremap <Leader><space> :call OmniSharp#GetCodeActions('visual')<cr>

" rename with dialog
nnoremap <Leader>nm :OmniSharpRename<cr>
nnoremap <F2> :OmniSharpRename<cr>
" rename without dialog - with cursor on the symbol to rename... ':Rename newname'
command! -nargs=1 Rename :call OmniSharp#RenameTo("<args>")

" Force OmniSharp to reload the solution. Useful when switching branches etc.
nnoremap <Leader>rl :OmniSharpReloadSolution<cr>
nnoremap <Leader>cf :OmniSharpCodeFormat<cr>
" Load the current .cs file to the nearest project
nnoremap <Leader>p :OmniSharpAddToProject<cr>

" (Experimental - uses vim-dispatch or vimproc plugin) - Start the omnisharp server for the current solution
" nnoremap <Leader>ss :OmniSharpStartServer<cr>
" nnoremap <Leader>sp :OmniSharpStopServer<cr>

" Add syntax highlighting for types and interfaces
nnoremap <Leader>h :OmniSharpHighlightTypes<cr>

" OmniSharp augroup {{{
augroup omnisharp_commands
    autocmd!

    autocmd FileType cs nnoremap <Leader>b :wa!<cr>:OmniSharpBuildAsync<cr>
    " automatic syntax check on events (TextChanged requires Vim 7.4)
    autocmd BufEnter,TextChanged,InsertLeave *.cs ALELint

    " Automatically add new cs files to the nearest project on save
    autocmd BufWritePost *.cs call OmniSharp#AddToProject()

    "show type information automatically when the cursor stops moving
    autocmd CursorHold *.cs call OmniSharp#TypeLookupWithoutDocumentation()

    "The following commands are contextual, based on the current cursor position.

    autocmd FileType cs nnoremap gd :OmniSharpGotoDefinition<cr>
    autocmd FileType cs nnoremap <Leader>fi :OmniSharpFindImplementations<cr>
    autocmd FileType cs nnoremap <Leader>ft :OmniSharpFindType<cr>
    autocmd FileType cs nnoremap <Leader>fs :OmniSharpFindSymbol<cr>
    autocmd FileType cs nnoremap <Leader>fu :OmniSharpFindUsages<cr>
    "finds members in the current buffer
    autocmd FileType cs nnoremap <Leader>fm :OmniSharpFindMembers<cr>
    " cursor can be anywhere on the line containing an issue
    autocmd FileType cs nnoremap <Leader>x  :OmniSharpFixIssue<cr>
    autocmd FileType cs nnoremap <Leader>fx :OmniSharpFixUsings<cr>
    " autocmd FileType cs nnoremap <Leader>tt :OmniSharpTypeLookup<cr>
    autocmd FileType cs nnoremap <Leader>dc :OmniSharpDocumentation<cr>
    "navigate up by method/property/field
    autocmd FileType cs nnoremap <C-K> :OmniSharpNavigateUp<cr>
    "navigate down by method/property/field
    autocmd FileType cs nnoremap <C-J> :OmniSharpNavigateDown<cr>
augroup END
