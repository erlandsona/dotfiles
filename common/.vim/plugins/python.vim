Plug 'davidhalter/jedi-vim'
Plug 'python-mode/python-mode'
Plug 'jmcomets/vim-pony'

"###########################
" Python Syntax Options
"###########################
let python_highlight_all = 1

"###########################
" PyMode Options
"###########################


" Activate rope
" Keys:
" K             Show python docs
" <Ctrl-Space>  Rope autocomplete
" <Ctrl-c>g     Rope goto definition
" <Ctrl-c>d     Rope show documentation
" <Ctrl-c>f     Rope find occurrences
" <Leader>b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operator modes)
let g:pymode_rope = 0

" Documentation
let g:pymode_doc = 1
let g:pymode_doc_key = ''

"Linting
let g:pymode_lint = 1
let g:pymode_lint_checker = "pyflakes,pep8"
" Auto check on save
let g:pymode_lint_write = 1
let g:pymode_lint_on_fly = 1
" Get rid of linting quickfix window.
let g:pymode_lint_cwindow = 0

" Support virtualenv
let g:pymode_virtualenv = 1

" Enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_bind = '<Leader>b'

" syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

" Don't autofold code
let g:pymode_folding = 0

"###########################
" Jedi-Vim Options
"###########################
let g:jedi#use_splits_not_buffers = "left"
let g:jedi#completions_command = "<c-n>"
let g:jedi#usages_command = "<Leader>u"
let g:jedi#completions_enabled = 0
let g:jedi#documentation_command = "K"

