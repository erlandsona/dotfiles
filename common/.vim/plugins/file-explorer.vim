" Plug 'justinmk/vim-dirvish' " Just broken for some reason?
Plug 'tpope/vim-vinegar'
" let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro' " defaults
let g:netrw_cursor = 2
let g:netrw_fastbrowse = 2 " Cache stuff, manually refresh <ctrl>-l to see new files.
let g:netrw_liststyle = 1 " ls -la
let g:netrw_localrmdir="rm -r"
let g:netrw_banner = 0 " Turn off banner
" Sort Directories next to similarly named Files
let g:netrw_sort_sequence='\<core\%(\.\d\+\)\=\>,\.h$,\.c$,\.cpp$,\~\=\*$,*,\.o$,\.obj$,\.info$,\.swp$,\.bak$,\~$'
