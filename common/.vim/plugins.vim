call plug#begin()


"{{{ Unused
" {{{ Python
" source ~/.vim/plugins/python.vim
" }}}
" {{{ C#
" source ~/.vim/plugins/csharp.vim
" }}}
"}}}

" {{{ CSS / Preprocessors
Plug 'JulesWang/css.vim', { 'for': 'css' }
Plug 'hail2u/vim-css3-syntax', { 'for': 'css' }
" Plug 'groenewege/vim-less'
" }}}

" {{{ Elm
Plug 'elmcast/elm-vim', { 'for': 'elm' }
" }}}

" {{{ Haskell
" Plug 'lukerandall/haskellmode-vim'
"
Plug 'parsonsmatt/vim2hs', { 'for': 'hs' }
let g:haskell_conceal = 0

Plug 'eagletmt/ghcmod-vim', { 'for': 'hs' }
Plug 'hspec/hspec.vim', { 'for': 'hs' }
Plug 'Twinside/vim-syntax-haskell-cabal', { 'for': 'hs' }
Plug 'pbrisbin/vim-syntax-shakespeare', { 'for': ['lucius', 'cassius', 'julius'] }
" }}}

" {{{ Html / Preprocessors
" Plug 'digitaltoad/vim-pug'
Plug 'slim-template/vim-slim', { 'for': 'slim' }
Plug 'mustache/vim-mustache-handlebars', { 'for': 'handlebars' }
" }}}

" {{{ PHP
Plug 'vim-vdebug/vdebug', { 'for': 'php' }
autocmd! User vdebug
  \   let g:vdebug_options['max_children'] = 128
  \ | let g:vdebug_options['break_on_open'] = 0
  \ | let g:vdebug_options['watch_window_style'] = 'compact'

Plug 'roxma/python-support.nvim' " run :PythonSupportInitPython3 for VDebug to work.
" Configure python_support not to check for Python2 support
let g:python_support_python2_require = 0

Plug 'StanAngeloff/php.vim', { 'for': 'php' }
Plug '2072/PHP-Indenting-for-VIm', { 'for': 'php' }
Plug 'jwalton512/vim-blade', { 'for': 'php' }
" }}}

" {{{ Ruby
Plug 'tpope/vim-bundler', { 'for': 'ruby' }
Plug 'tpope/vim-rails', { 'for': 'ruby' }
Plug 'tpope/vim-rake', { 'for': 'ruby' }
Plug 'vim-ruby/vim-ruby', { 'for': 'ruby' }
" Significantly speed up vim-ruby / vim-rails / vim-rake etc...
let g:ruby_path = []
autocmd BufNewFile,BufRead *_test.rb set syntax=rspec

" Compatibility with vim8 might be bad...
" Plug 'vim-scripts/ruby-matchit'
" Maybe more up-to-date version?
Plug 'tmhedberg/matchit'
Plug 'keith/rspec.vim', { 'for': 'ruby' }
"}}}

" {{{ Javascript
" Plug 'jelera/vim-javascript-syntax' " Outdated?
"
" Plug 'pangloss/vim-javascript' " Outdated?
" let g:javascript_enable_domhtmlcss = 1
"
Plug 'othree/yajs.vim', { 'for': 'javascript' } " Yet another javascript syntax 'Highlighting'
Plug 'othree/es.next.syntax.vim', { 'for': 'javascript' } " For ES2015 / ES7 support
Plug 'HerringtonDarkholme/yats.vim', { 'for': 'typescript' }            " Yet another typescript syntax 'Highlighting'

Plug 'othree/javascript-libraries-syntax.vim', { 'for': 'javascript' }  " For JS Libs => ng, _, $, etc...
let g:used_javascript_libs = 'jquery,underscore,backbone,angularjs,angularui,react,flux,requirejs,sugar,jasmine,chai,handlebars'
let g:jsx_ext_required = 0 " Allow JSX in normal JS files

Plug 'kchmck/vim-coffee-script', { 'for': 'coffee' } " Coffeescript syntax
Plug 'claco/jasmine.vim', { 'for': ['javascript', 'coffee'] }
Plug 'mxw/vim-jsx', { 'for': 'jsx' }
Plug 'Quramy/tsuquyomi'
Plug 'Quramy/vim-js-pretty-template'
Plug 'clausreinke/typescript-tools.vim', { 'for': 'typescript', 'do': 'npm install' }
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }
"}}}

" {{{ Json
"
Plug 'tpope/vim-jdaddy', { 'for': 'json' } " JSON
Plug 'elzr/vim-json', { 'for': 'json' } " JSON Syntax Highlighting
"}}}

" {{{ Nginx
" Borken on Nvim?
" Plug 'evanmiller/nginx-vim-syntax'
Plug 'chr4/nginx.vim'
"}}}

" {{{ General
" source ~/.vim/plugins/syntastic.vim
Plug 'w0rp/ale' " Async Linting Engine... successor to syntastic?
let g:ale_virtualtext_cursor = 0

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
let g:deoplete#enable_at_startup = 1

Plug 'Raimondi/delimitMate'
Plug 'Shougo/vimproc.vim', { 'do' : 'make' } " Needed for TypeScript stuff?
Plug 'tpope/vim-dispatch'
Plug 'terryma/vim-multiple-cursors'

Plug 'airblade/vim-gitgutter'
let g:gitgutter_max_signs = 2000

source ~/.vim/plugins/file-explorer.vim

Plug 'tpope/vim-commentary'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-git'
Plug 'michaeljsmith/vim-indent-object'


source ~/.vim/plugins/sessions.vim

Plug 'tpope/vim-repeat'
Plug 'honza/vim-snippets'
Plug 'tpope/vim-surround'
source ~/.vim/plugins/test.vim
Plug 'wellle/targets.vim' " Extra Text Objects

source ~/.vim/plugins/fzf.vim

Plug 'ludovicchabant/vim-gutentags'
Plug 'majutsushi/tagbar'
Plug 'kana/vim-textobj-user'
Plug 'glts/vim-textobj-comment'
Plug 'nelstrom/vim-textobj-rubyblock', { 'for': ['ruby', 'erb', 'rspec'] }

" source ~/.vim/plugins/indent-guides.vim
" source ~/.vim/plugins/tmux-navigator.vim

Plug 'troydm/zoomwintab.vim'
nnoremap <A-z> :ZoomWinTabToggle<return>
tnoremap <A-z> <C-\><C-n>:ZoomWinTabToggle<return>i

Plug 'ntpeters/vim-better-whitespace'
Plug 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 0
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']
" Plug 'edkolev/tmuxline.vim'
"##########################

call plug#end() " initialize plugin system
" vim:foldmethod=marker:foldlevel=0
