" Austin Erlandson's .vimrc
set nocompatible

" Auto install vim-plug
let plug_path = has('nvim') ?
      \ '~/.local/share/nvim/site/' :
      \ '~/.vim/'

if empty(glob(plug_path . "autoload/plug.vim"))
  execute "! curl -fLo " . plug_path . "autoload/plug.vim --create-dirs"
    \ . " https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let mapleader = ","

colorscheme peachpuff

source ~/.vim/sets.vim
source ~/.vim/mappings.vim
source ~/.vim/plugins.vim
source ~/.vim/highlight.vim
source ~/.vim/plugins/statusline.vim
