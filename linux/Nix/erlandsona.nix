# Symlink this to .config/nixpkgs/config.nix and
# run `nix-env -irA nixos.erlandsona` to install all the things.
# this assumes you've got a nix-channel --list
#=> master https://whatevernix channel link.

{ pkgs ? (import <nixpkgs> {})
, system ? builtins.currentSystem
}:
{
  allowUnfree = true;
  /* allowBroken = true; */
  /* chromium = { */
  /*   enablePepperFlash = true; */
  /*   enablePepperPDF = true; */
  /*   icedtea=true; */
  /* }; */
  /* firefox = { */
  /*   enableGoogleTalkPlugin = true; */
  /*   /1* enableAdobeFlash = true; *1/ */
  /*   icedtea=true; */
  /* }; */

  permittedInsecurePackages = [
    # For gnome3.geary
    /* "webkitgtk-2.4.11" */
  ];
  packageOverrides = pkgs: with pkgs; rec {
    erlandsona = with pkgs; buildEnv {
      name = "erlandsona";
      paths = [
        # DAW
          ardour
          bitwig-studio
        # Plugins
          # Audio Plugins
            AMB-plugins
            calf
            caps
            distrho
            eq10q
            mda_lv2
          # Plugin Gui's
            suil-qt5
          # Guitar
            guitarix # Amp Sim
            rakarrack # Pedalboard
          # Drums
            drumgizmo
            drumkv1
            drumstick
            hydrogen



        # Browsers
          # chromium               # Trying to get Netflix to work.
          firefox google-chrome
          /* hal-flash # For NPAPI / Flash support in Google Chrome */

        # Chat Clients
          # hipchat
          slack
          konversation
          # Terminal IRC
          # weechat
          # Floating Window IRC
          # pidgin

        # For Ruby
          bundler
          # ruby_2_2               # Ruby
          ruby
          readline

        cabal-install


        # For Javascript
          nodejs-8_x
          nodePackages.node2nix
          # yarn

        ctags

        # Disk Partitioning
          gptfdisk

        elmPackages.elm        # elm lang?
        elmPackages.elm-compiler
        elmPackages.elm-format
        elmPackages.elm-make
        elmPackages.elm-package
        elmPackages.elm-reactor
        elmPackages.elm-repl
        gcc
        gdb                    # debugging tool?
        gitAndTools.gitFull    # Git-CLI
        # gnome3.gnome-bluetooth # bluetooth gui cause cli bluetooth doesn't make sense.
        # gnome3.gnome-tweak-tool

        gmp
        gnumake
        granite                # Widget Theme?

        stack # for Haskell projects :shrug:
        haskellPackages.ghc    # Haskell Compiler
        haskellPackages.xmobar # Use with trayer?

        imagemagick # CLI ImageManipulation
        inkscape # SVG / Font Editor...
        gimp # PhotoShop for X???

        # Archive Tools
          ark

        # bluetooth gui cause cli bluetooth doesn't make sense.
          bluedevil

        # Disk Utility
          gparted # Graphical Disk Utility
          smartmontools # Internal Drive Info

        # File Managers
          pcmanfm # pantheon is better but not in Nix yet.

        # PDF Viewers
          okular

        # Email Client
          thunderbird
          # :( No worky with POP3...
          /* gnome3.geary */

        # Patch ELF binaries.
          patchelf
          chrpath

        # Extra info about files.
          file

        # May be needed to run elm-make?
          /* glibc_multi */

        # Better nix-env?
        nox
        nix-prefetch-git

        libxml2
        libxslt

        lsof                   # list open files
        # App Theme Previewer
          lxappearance

        lzma
        lzip
        man-pages
        nix-repl

        openssl

        # For lspci & killall resp.
          pciutils
          psmisc

        # ScreenRecording with Keyboard Hinting?
        /* python27Packages.screenkey */
        /* python27Packages.xlib */

        docker_compose

        # Screen Sharing
          /* skype */

        # Music Player
          spotify

        # Database
          sqlite

        # Admin Privaleges
          sudo
        # tint2                  # App Menu Bar...
        trayer                 # Toolbar
        tree                   # list in tree format files...
        unzip                  # decompress .zip extensions
        usbutils

        cmake # Required for YCM
        neovim
        python3Full

        vlc                    # Media Player
        wget                   # Similar to CURL / git
        xclip                  # Clipboard
        xfontsel               # For better fonts with dmenu
        fontmatrix
        /* xkeyboard_config */ # xkb conflicts?
        xorg.xclock
        xorg.xdpyinfo
        xorg.xev               # Track Key code output from keyboard.
        xorg.xmessage          # X Reporter?
        xorg.xmodmap           # Remap CapsLock to Alt!
        zlib

        ag                     # The Silver Searcher... like grep but better?

        # Launchers
          albert                 # dmenu alternative.
          dmenu2                  # NCurses looking App Launcher
          # rofi # Albert does a fine job for now.
          /* synapse zeitgeist # Needed for Synapse */
        # Display Server?
          compton                # To fix transparency issues

        acpi # for battery_percentage helper.

        alsa-firmware
        alsaLib
        alsaPlugins
        alsaTools              # hda-jack-retask for fixing audio ports?
        alsaUtils
        a2jmidid
        qjackctl

        arandr                 # XRandR GUI for configuring Multiple Monitors

      # Login Manager
          lightdm
          lightlocker            # Send to Lightdm Locker
          xscreensaver           # Screensaver

        # QT Theme Resources?
        gnome-breeze # Aparently kde5 breeze is broken?
        qt5ct
        /* breeze-qt5 */
        /* breeze-icons */

        # GTK Engine...
          /* webkitgtk2 */
          /* gtk-engine-murrine */
          gnome3.webkitgtk
        # GTK Theme Resources?
          adapta-gtk-theme
          arc-theme
          materia-theme
          /* paper-gtk-theme */

        # Icon Theme Resources
                    # Fallbacks
          arc-icon-theme material-icons moka-icon-theme gnome3.adwaita-icon-theme hicolor_icon_theme

          /* paper-icon-theme */

        /* qt5.qtbase.gtk */
        qt5.full
        libsForQt5.polkit-qt
        libsForQt5.qtstyleplugins

        # Terminal Emulators
        konsole rxvt_unicode

      ];
    };
  };
}
