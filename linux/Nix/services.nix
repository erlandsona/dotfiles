{ config, expr, pkgs, ...}:
{
  # List services that you want to enable:
  services = {
    # Enable dbus for hdajackretask
    dbus.enable = true;

    # Databases for Development
    # use Docker for these...
    /* mysql = { */
    /*   enable = true; */
    /*   package = pkgs.mariadb; */
    /* }; */
    /* postgresql = { */
    /*   enable = true; */
    /*   package = pkgs.postgresql; */
    /* }; */
    /* rabbitmq.enable = true; */

    # Enable the OpenSSH daemon.
    openssh.enable = true;
    # Enable CUPS to print documents.
    # printing.enable = true;

    # Redshift is F.Lux for Linux
    redshift = {
      enable = true;
      brightness = {
        day = "1.0";
        night = "0.75";
      };
      temperature = {
        day = 5777;
        night = 4777;
      };
      latitude = "0";
      longitude = "0";
    };

    # Enable the X11 windowing system.
    xserver = {
      enable = true;
      layout = "us";
      /* xkbModel = "acer_laptop"; # Try apple_laptop too? */
      /* xkbOptions = "altwin:swap_lalt_lwin"; */
      xkbVariant = "altgr-intl";

      /* desktopManager.gnome3.enable = true; # For GNome */

      /* desktopManager.kde4.enable = true; # For KDE4 */
      /* desktopManager.kde5.enable = true; # For KDE5 */
      # For XMonad
      desktopManager.xterm.enable = false;
      windowManager = {
        default = "xmonad";
        xmonad.enable = true;
        xmonad.enableContribAndExtras = true;
      };
      displayManager = {
        lightdm = {
          enable = true;
          background = "/data/pictures/wallpapers/space-purple-nebula.jpg";
          greeters.gtk = {
            iconTheme.name = "Material";
            iconTheme.package = pkgs.material-icons;
            theme.name = "Adapta";
            theme.package = pkgs.adapta-gtk-theme;
          };
        };
        sessionCommands = ''
          ${pkgs.feh}/bin/feh --bg-fill /data/pictures/wallpapers/space-purple-nebula.jpg
          ${pkgs.xlibs.xrdb}/bin/xrdb -load /home/erlandsona/.Xresources

          # ${pkgs.networkmanagerapplet}/bin/nm-applet &

          ${pkgs.xorg.xsetroot}/bin/xsetroot -cursor_name left_ptr &

          ${pkgs.compton}/bin/compton -b

          # Make Caps_Lock => Alt
          ${pkgs.xorg.xmodmap}/bin/xmodmap -e "clear lock"
          ${pkgs.xorg.xmodmap}/bin/xmodmap -e "clear mod1"
          ${pkgs.xorg.xmodmap}/bin/xmodmap -e "keysym Caps_Lock = Alt_L"
          ${pkgs.xorg.xmodmap}/bin/xmodmap -e "add mod1 = Alt_L"

          ${pkgs.xscreensaver}/bin/xscreensaver -no-splash &
          ${pkgs.lightlocker}/bin/light-locker \
                                  --lock-after-screensaver=60 \
                                  --lock-on-suspend &
        '';
      };
      synaptics = {
        enable = true;

        accelFactor = "0.075"; # 0.001 is default
        minSpeed = "0.25"; # 0.6 is default
        /* maxSpeed = "1.0"; # 1.0 is default */

        tapButtons = false;

        twoFingerScroll = true;
        scrollDelta = -137;

        additionalOptions = ''
          Option "CoastingSpeed" "7"
          Option "CoastingFriction" "70"
          Option "SoftButtonAreas" "0 0 0 0 0 0 0 0"
        '';
      };
    };
  };
}
