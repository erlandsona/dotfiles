# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      <musnix>
      ./services.nix
      /* ./lightdm-paper-icons.nix */
    ];


  # Maybe convert to using Grub bootloader sometime?
  # boot.grubSplashImage = /home/erlandsona/Downloads/space-purple-nebula_640x480.png

  # Use the gummiboot efi boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.timeout = 1;

  boot.kernelModules = [
    "coretemp"
    "nvme"

    "snd-hda-intel"
    "snd-virmidi"
  ];
  boot.extraModprobeConfig = ''
    # This line comes from the program 'hda-jack-retask'.
    options snd-hda-intel patch=hda-jack-retask.fw,hda-jack-retask.fw,hda-jack-retask.fw,hda-jack-retask.fw
  '';

  # Add back if you notice screen tearing.
  /* services.xserver.videoDrivers = [ "modesetting" ]; */
  hardware.opengl.driSupport32Bit = true;

  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment = {
    systemPackages = with pkgs; [
      # Jack Connection Kit for Realtime Audio Configuation?
        jack2Full
        pavucontrol # PulseAudio VU Metering - Like SystemPref's Audio Tab.

        # ??? not sure if this is working?
        thunderbolt
    ];

    # Only add exports here... these happen on every new instance of Bash.
    extraInit = ''
      # GTK3: remove local user overrides (for determinisim, causes hard to find bugs)
      rm -f ~/.config/gtk-3.0/settings.ini

      export XDG_DATA_DIRS="${pkgs.adapta-gtk-theme}/share:$XDG_DATA_DIRS"

      # GTK3: add /etc/xdg/gtk-3.0 to search path for settings.ini
      # We use /etc/xdg/gtk-3.0/settings.ini to set the icon and theme name for GTK 3
      export XDG_CONFIG_DIRS="/etc/xdg:$XDG_CONFIG_DIRS"

      # GTK2 theme + icon theme
      export GTK2_RC_FILES=${pkgs.writeText "iconrc" ''gtk-icon-theme-name="Arc"''}:${pkgs.adapta-gtk-theme}/share/themes/Adapta/gtk-2.0/gtkrc:$GTK2_RC_FILES

      # SVG loader for pixbuf (needed for GTK svg icon themes)
      export GDK_PIXBUF_MODULE_FILE=$(echo ${pkgs.librsvg.out}/lib/gdk-pixbuf-2.0/*/loaders.cache)


      export GTK_DATA_PREFIX=${config.system.path}
      export GIO_EXTRA_MODULES=${pkgs.xfce.gvfs}/lib/gio/modules

      # these are the defaults, but some applications are buggy so we'll set them
      # here anyway
      export XDG_CONFIG_HOME=$HOME/.config
      export XDG_DATA_HOME=$HOME/.local/share
      export XDG_CACHE_HOME=$HOME/.cache

      # For QT Applications
      export QT_QPA_PLATFORMTHEME="qt5ct"
      export QT_STYLE_OVERRIDE=gtk2
    '';

    # GTK3 global theme (widget and icon theme)
    etc."xdg/gtk-3.0/settings.ini" = {
      text = ''
        [Settings]
        gtk-icon-theme-name=Arc
        gtk-theme-name=Adapta
      '';
      mode = "444";
    };
    pathsToLink = [ "/share" ];
  };

  fonts = {
    enableDefaultFonts = true;
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      # Regular System Fonts
      caladea

      cantarell_fonts
      dejavu_fonts

      emojione

      font-awesome-ttf # ???

      font-droid # Googles Android Fonts

      fira # Sans Version
      fira-code # Code Font with Ligatures :)

      league-of-moveable-type

      /* noto-fonts-emoji */
      /* noto-fonts */
    ];
  };

  hardware.bluetooth.enable = true;

  # Fixes Sound Card for 17" Acer Predator.
  hardware.firmware = [
    (pkgs.runCommand "hda-jack-retask" {} ''
      mkdir -pv $out/lib/firmware
      cp -vi ${./hda-jack-retask.fw} $out/lib/firmware/hda-jack-retask.fw
    '')
  ];
  # Allows hdajackretask to authenticate properly.
  security.polkit.enable = true;

  # Select internationalisation properties.
  i18n = {
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  /* boot.kernelPackages = pkgs.linuxPackages_latest; */
  musnix = {
    enable = true;
    alsaSeq.enable = true;
    kernel = {
      optimize = true;
      packages = pkgs.linuxPackages_latest_rt;
      # NVidia Drivers don't work with PREEMPT_RT Patch.
      realtime = true;
    };
    soundcardPciId = "00:1f.3";
    rtirq.enable = true; # Realtime Thread Scheduler
  };

  # Some programs need pulseaudio, I think maybe chrome?
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };

  # Alsa sound?
  sound = {
    enable = true;
    enableOSSEmulation = false;
    mediaKeys.enable = true;
  };

  networking.hostName = "erlandsona"; # Define your hostname.
  networking.firewall = {
    # Disabled due to Chromecast problem
    # https://github.com/NixOS/nixpkgs/issues/3107
    enable = false;
    allowedTCPPorts = [ 80 443 22 5556 ];
    allowedUDPPorts = [ 5556 ];
  };

  # networking.wireless.enable = true; # Enables wireless support via wpa_supplicant.
  networking.enableIPv6 = false;
  networking.networkmanager.enable = true;  # Enables wireless support via networkmanager.

  programs = {
    kbdlight.enable = true;
    light.enable = true;
    man.enable = true;
    tmux = {
      baseIndex = 1;
      clock24 = true;
      enable = true;
      escapeTime = 0;
      /* keyMode = "vi"; */
      /* newSession = true; */
      terminal = "screen-256color";
    };
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  /* system.stateVersion = "16.09"; */

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.mutableUsers = false;
  users.users.erlandsona = {
    description = "Austin Erlandson";
    extraGroups = [ "wheel" "audio" "networkmanager" "lp" "dbus" "docker" ];
    isNormalUser = true;
  };

  virtualisation.docker.enable = true;

}
