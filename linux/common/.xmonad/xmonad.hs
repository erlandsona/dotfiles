-- Imports {{{
-- Utility Functions
import Control.Arrow (second)
import Data.List (concatMap)


-- XMonad & Contrib Modules
import XMonad
-- import XMonad.Actions.CycleRecentWS
import XMonad.Actions.GroupNavigation (nextMatch, Direction(Backward, Forward))
-- import XMonad.Actions.Navigation2D
import XMonad.Config.Desktop
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.IndependentScreens (countScreens, withScreens)
import XMonad.Layout.ResizableTile
  ( ResizableTall(..)
  -- , MirrorResize(MirrorExpand, MirrorShrink)
  )
import XMonad.Layout.Spacing
import XMonad.Util.EZConfig (additionalKeysP, checkKeymap, removeKeysP)
import qualified XMonad.StackSet as W
-- }}}

main = countScreens >>= xmonad . myConfig

myConfig monitors = desktopConfig
  { borderWidth       = 0
  , focusFollowsMouse = False
  , layoutHook        = ResizableTall 1 (3/100) (2/3) [] ||| Full
  , modMask           = mod4Mask -- Super
  , startupHook       = myStartupHook monitors
  , terminal          = "konsole"
  , workspaces        = withScreens monitors ["One"]
  } `removeKeysP` defaultKeys
    `additionalKeysP` myKeys


myStartupHook monitors = do
  checkKeymap (myConfig monitors) myKeys
  spawn $ "xmessage " ++ (show $ toInteger monitors)
  spawn $ "albert"

-- Keybindings {{{
defaultKeys =
      [ "M-" ++ [char] | char <- "nm,./qwerhjkltp" ]
  ++ ["M-S-" ++ [char] | char <- "qwerjkcp" ]
  ++ ["M-<Return>", "M-S-<Return>"]


myKeys =
  [ "M-S-\\"      ! (withFocused $ windows . W.sink)
  , "M-]"         ! sendMessage Expand -- Horizontal Sizing
  , "M-["         ! sendMessage Shrink -- Horizontal Sizing
  -- , "M-]"         ! sendMessage MirrorExpand -- Vertical Sizing
  -- , "M-["         ! sendMessage MirrorShrink -- Vertical Sizing
  , "M-S-]"       ! windows W.swapDown
  , "M-S-["       ! windows W.swapUp
  , "M-z"         ! sendMessage NextLayout
  -- XMonad.Actions.CycleRecentWS
  -- XMonad.Actions.GroupNavigation
  , "M-<Tab>"     ! nextApp Forward
  , "M-S-<Tab>"   ! nextApp Backward
  , "M-`"         ! nextWindow Forward
  , "M-S-`"       ! nextWindow Backward
  , "M-S-k"       ! kill
  , "M-<Return>"  ! spawn "konsole"
  ] ++ map (second spawn)
  [ "<XF86MonBrightnessUp>"  ! "light -Sr $(expr $(light -Gr) '*' 2 + 1)"
  , "<XF86MonBrightnessDown>"! "light -Sr $(expr $(light -Gr) / 2)"
  , "<XF86AudioMute>"        ! volume toggle
  , "<XF86AudioRaiseVolume>" ! volume louder
  , "<XF86AudioLowerVolume>" ! volume quieter
  , "M-<Space>"              ! "albert toggle"
  , "M-S-<Space>"            ! dmenu_toggle
  -- , "M-<Space>"              ! "synapse"
  -- RE Scr'ee'n Lock
  , "<Scroll_lock>"          ! "light-locker-command -l"
  ]
-- }}}

-- Volume functions {{{
-- should work across multiple soundcards
volume :: String -> String
volume action = concatMap commandString cardNumbers
  where
    commandString =
      (("amixer set Master " ++ action ++ " -c ") ++) . (++ "; ") . show


cardNumbers :: [Int]
cardNumbers = [0,1]


toggle :: String
toggle = "toggle"


louder :: String
louder = "3dB+ unmute"


quieter :: String
quieter = "3dB- unmute"
--}}}


dmenu_toggle :: String
dmenu_toggle = "dmenu_run -i -o 0.93 -l 10 -h 30 -uh 3 -fn 'Fira Sans:size=15' -x 500 -y 500 -w 1000 -nb '#370037' -nf '#AEAE00' -sb '#170077' -sf '#EAEA00'"


-- Utility Functions {{{

-- Creates a nicer Hash-like syntax...
(!) = ((,))

-- Taken from Actions.GroupNavigation and modified to mimic OSX Window Switcher.
nextApp :: Direction -> X ()
nextApp dir = withFocused $ \win -> do
  prop <- runQuery appName win
  nextMatch dir (appName /=? prop)

nextWindow :: Direction -> X ()
nextWindow dir = withFocused $ \win -> do
  prop <- runQuery appName win
  nextMatch dir (appName =? prop)
-- }}}

-- vim:foldmethod=marker:foldlevel=0
