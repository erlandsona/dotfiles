[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=erlandsona
Font=FuraCode Nerd Font Mono,11,-1,5,50,0,0,0,0,0
LineSpacing=0
UseFontLineChararacters=true

[Cursor Options]
CursorShape=0

[General]
Icon=utilities-terminal
Name=erlandsona
Parent=FALLBACK/

[Scrolling]
HistoryMode=2
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=false
